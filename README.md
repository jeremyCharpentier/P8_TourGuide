![](img/TourGuide.png)
# TourGuide
**Tour Guide is a springboot application by Trip Master.**

------------------------

With this app, users can see near attractions and obtain discount for trips.

**Technicals :** 
- Java : 1.8
- Spring Boot : 2.7.0
- Gradle 
- Gitlab 
- Junit : 5.9.0
- Jacoco : 0.8.4

------------------------
**Diagram :**

![](img/diagramme.jpg)
