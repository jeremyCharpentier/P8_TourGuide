package tourGuide;

import gpsUtil.GpsUtil;
import gpsUtil.location.VisitedLocation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import rewardCentral.RewardCentral;
import tourGuide.domain.User;
import tourGuide.dto.NearAttractionsDto;
import tourGuide.helper.InternalTestHelper;
import tourGuide.service.AttractionServiceImpl;
import tourGuide.service.RewardsServiceImpl;
import tourGuide.service.UserServiceImpl;
import tripPricer.Provider;

import java.util.List;
import java.util.Locale;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class TestUserServiceImpl {
    @BeforeEach
    public void init() {
        Locale.setDefault(new Locale("en", "US"));
    }

    @Test
    public void getUserLocation() {
        GpsUtil gpsUtil = new GpsUtil();
        RewardCentral rewardCentral = new RewardCentral();
        AttractionServiceImpl attractionServiceImpl = new AttractionServiceImpl();
        RewardsServiceImpl rewardsServiceImpl = new RewardsServiceImpl(rewardCentral, attractionServiceImpl);

        InternalTestHelper.setInternalUserNumber(0);
        UserServiceImpl userServiceImpl = new UserServiceImpl(gpsUtil, rewardsServiceImpl, attractionServiceImpl);

        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        VisitedLocation visitedLocation = userServiceImpl.trackUserLocation(user);
        userServiceImpl.tracker.stopTracking();
        assertEquals(visitedLocation.userId, user.getUserId());
    }

    @Test
    public void addUser() {
        GpsUtil gpsUtil = new GpsUtil();
        RewardCentral rewardCentral = new RewardCentral();
        AttractionServiceImpl attractionServiceImpl = new AttractionServiceImpl();
        RewardsServiceImpl rewardsServiceImpl = new RewardsServiceImpl(rewardCentral, attractionServiceImpl);

        InternalTestHelper.setInternalUserNumber(0);
        UserServiceImpl userServiceImpl = new UserServiceImpl(gpsUtil, rewardsServiceImpl, attractionServiceImpl);

        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        User user2 = new User(UUID.randomUUID(), "jon2", "000", "jon2@tourGuide.com");

        userServiceImpl.addUser(user);
        userServiceImpl.addUser(user2);

        User retrievedUser = userServiceImpl.getUser(user.getUserName());
        User retrievedUser2 = userServiceImpl.getUser(user2.getUserName());

        userServiceImpl.tracker.stopTracking();

        assertEquals(user, retrievedUser);
        assertEquals(user2, retrievedUser2);
    }

    @Test
    public void getAllUsers() {
        GpsUtil gpsUtil = new GpsUtil();
        RewardCentral rewardCentral = new RewardCentral();
        AttractionServiceImpl attractionServiceImpl = new AttractionServiceImpl();
        RewardsServiceImpl rewardsServiceImpl = new RewardsServiceImpl(rewardCentral, attractionServiceImpl);

        InternalTestHelper.setInternalUserNumber(0);
        UserServiceImpl userServiceImpl = new UserServiceImpl(gpsUtil, rewardsServiceImpl, attractionServiceImpl);

        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        User user2 = new User(UUID.randomUUID(), "jon2", "000", "jon2@tourGuide.com");

        userServiceImpl.addUser(user);
        userServiceImpl.addUser(user2);

        List<User> allUsers = userServiceImpl.getAllUsers();

        userServiceImpl.tracker.stopTracking();

        assertTrue(allUsers.contains(user));
        assertTrue(allUsers.contains(user2));
    }

    @Test
    public void trackUser() {
        GpsUtil gpsUtil = new GpsUtil();
        RewardCentral rewardCentral = new RewardCentral();
        AttractionServiceImpl attractionServiceImpl = new AttractionServiceImpl();
        RewardsServiceImpl rewardsServiceImpl = new RewardsServiceImpl(rewardCentral, attractionServiceImpl);

        InternalTestHelper.setInternalUserNumber(0);
        UserServiceImpl userServiceImpl = new UserServiceImpl(gpsUtil, rewardsServiceImpl, attractionServiceImpl);

        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        VisitedLocation visitedLocation = userServiceImpl.trackUserLocation(user);

        userServiceImpl.tracker.stopTracking();

        assertEquals(user.getUserId(), visitedLocation.userId);
    }

    @Test
    public void getFiveClosesAttractions() {
        //GIVEN
        GpsUtil gpsUtil = new GpsUtil();
        RewardCentral rewardCentral = new RewardCentral();
        AttractionServiceImpl attractionServiceImpl = new AttractionServiceImpl();
        RewardsServiceImpl rewardsServiceImpl = new RewardsServiceImpl(rewardCentral, attractionServiceImpl);

        InternalTestHelper.setInternalUserNumber(0);
        UserServiceImpl userServiceImpl = new UserServiceImpl(gpsUtil, rewardsServiceImpl, attractionServiceImpl);

        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        VisitedLocation visitedLocation = userServiceImpl.trackUserLocation(user);
        //WHEN
        List<NearAttractionsDto> attractions = userServiceImpl.getFiveClosesAttractions(visitedLocation);

        userServiceImpl.tracker.stopTracking();

        assertEquals(5, attractions.size());
    }

    @Test
    public void getTripDeals() {
        GpsUtil gpsUtil = new GpsUtil();
        RewardCentral rewardCentral = new RewardCentral();
        AttractionServiceImpl attractionServiceImpl = new AttractionServiceImpl();
        RewardsServiceImpl rewardsServiceImpl = new RewardsServiceImpl(rewardCentral, attractionServiceImpl);

        InternalTestHelper.setInternalUserNumber(1);
        UserServiceImpl userServiceImpl = new UserServiceImpl(gpsUtil, rewardsServiceImpl, attractionServiceImpl);

        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");

        List<Provider> providers = userServiceImpl.getTripDeals(user);

        userServiceImpl.tracker.stopTracking();

        assertEquals(5, providers.size());
    }


}
