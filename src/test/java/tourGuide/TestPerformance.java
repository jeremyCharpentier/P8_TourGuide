package tourGuide;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import rewardCentral.RewardCentral;
import tourGuide.domain.User;
import tourGuide.helper.InternalTestHelper;
import tourGuide.service.AttractionServiceImpl;
import tourGuide.service.RewardsServiceImpl;
import tourGuide.service.UserServiceImpl;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class TestPerformance {

    /*
     * A note on performance improvements:
     *
     *     The number of users generated for the high volume tests can be easily adjusted via this method:
     *
     *     		InternalTestHelper.setInternalUserNumber(100000);
     *
     *
     *     These tests can be modified to suit new solutions, just as long as the performance metrics
     *     at the end of the tests remains consistent.
     *
     *     These are performance metrics that we are trying to hit:
     *
     *     highVolumeTrackLocation: 100,000 users within 15 minutes:
     *     		assertTrue(TimeUnit.MINUTES.toSeconds(15) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
     *
     *     highVolumeGetRewards: 100,000 users within 20 minutes:
     *          assertTrue(TimeUnit.MINUTES.toSeconds(20) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
     */

    @BeforeEach
    public void init() {
        Locale.setDefault(new Locale("en", "US"));
    }

    @Test
    public void highVolumeTrackLocation() {
        GpsUtil gpsUtil = new GpsUtil();
        AttractionServiceImpl attractionServiceImpl = new AttractionServiceImpl();
        RewardCentral rewardCentral = new RewardCentral();
        RewardsServiceImpl rewardsServiceImpl = new RewardsServiceImpl(rewardCentral, attractionServiceImpl);
        // Users should be incremented up to 100,000, and test finishes within 15 minutes
        InternalTestHelper.setInternalUserNumber(100_000);
        UserServiceImpl userServiceImpl = new UserServiceImpl(gpsUtil, rewardsServiceImpl, attractionServiceImpl);

        List<User> allUsers = userServiceImpl.getAllUsers();

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        userServiceImpl.trackUserLocationRunnable(allUsers);

        stopWatch.stop();
        userServiceImpl.tracker.stopTracking();

        System.out.println("highVolumeTrackLocation: Time Elapsed: " + TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()) + " seconds.");
        Assertions.assertTrue(TimeUnit.MINUTES.toSeconds(15) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
    }

    @Test
    public void highVolumeGetRewards() {
        GpsUtil gpsUtil = new GpsUtil();
        AttractionServiceImpl attractionServiceImpl = new AttractionServiceImpl();
        RewardCentral rewardCentral = new RewardCentral();
        RewardsServiceImpl rewardsServiceImpl = new RewardsServiceImpl(rewardCentral, attractionServiceImpl);

        // Users should be incremented up to 100,000, and test finishes within 20 minutes
        InternalTestHelper.setInternalUserNumber(100_000);
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        UserServiceImpl userServiceImpl = new UserServiceImpl(gpsUtil, rewardsServiceImpl, attractionServiceImpl);

        Attraction attraction = attractionServiceImpl.getAttractions().get(0);
        List<User> allUsers = userServiceImpl.getAllUsers();
        allUsers.forEach(u -> u.addToVisitedLocations(new VisitedLocation(u.getUserId(), attraction, new Date())));

        rewardsServiceImpl.completableFutureCalculateRewards(allUsers);

        for (User user : allUsers) {
            Assertions.assertTrue(user.getUserRewards().size() > 0);
        }
        stopWatch.stop();
        userServiceImpl.tracker.stopTracking();

        System.out.println("highVolumeGetRewards: Time Elapsed: " + TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()) + " seconds.");
        Assertions.assertTrue(TimeUnit.MINUTES.toSeconds(20) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
    }

}
