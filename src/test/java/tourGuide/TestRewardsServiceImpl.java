package tourGuide;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import rewardCentral.RewardCentral;
import tourGuide.domain.User;
import tourGuide.domain.UserReward;
import tourGuide.helper.InternalTestHelper;
import tourGuide.service.AttractionServiceImpl;
import tourGuide.service.RewardsServiceImpl;
import tourGuide.service.UserServiceImpl;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

public class TestRewardsServiceImpl {
    @BeforeEach
    public void init() {
        Locale.setDefault(new Locale("en", "US"));
    }

    @Test
    public void userGetRewards() {
        GpsUtil gpsUtil = new GpsUtil();
        AttractionServiceImpl attractionServiceImpl = new AttractionServiceImpl();
        RewardCentral rewardCentral = new RewardCentral();
        RewardsServiceImpl rewardsServiceImpl = new RewardsServiceImpl(rewardCentral, attractionServiceImpl);

        InternalTestHelper.setInternalUserNumber(0);
        UserServiceImpl userServiceImpl = new UserServiceImpl(gpsUtil, rewardsServiceImpl, attractionServiceImpl);

        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        Attraction attraction = attractionServiceImpl.getAttractions().get(0);
        user.addToVisitedLocations(new VisitedLocation(user.getUserId(), attraction, new Date()));
        userServiceImpl.trackUserLocation(user);
        List<UserReward> userRewards = user.getUserRewards();
        userServiceImpl.tracker.stopTracking();

        Assertions.assertEquals(1, userRewards.size());
    }

    @Test
    public void isWithinAttractionProximity() {
        RewardCentral rewardCentral = new RewardCentral();
        AttractionServiceImpl attractionServiceImpl = new AttractionServiceImpl();
        RewardsServiceImpl rewardsServiceImpl = new RewardsServiceImpl(rewardCentral, attractionServiceImpl);

        Attraction attraction = attractionServiceImpl.getAttractions().get(0);
        Assertions.assertTrue(rewardsServiceImpl.isWithinAttractionProximity(attraction, attraction));
    }

    @Test
    public void nearAllAttractions() {
        GpsUtil gpsUtil = new GpsUtil();
        RewardCentral rewardCentral = new RewardCentral();
        AttractionServiceImpl attractionServiceImpl = new AttractionServiceImpl();
        RewardsServiceImpl rewardsServiceImpl = new RewardsServiceImpl(rewardCentral, attractionServiceImpl);

        rewardsServiceImpl.setProximityBuffer(Integer.MAX_VALUE);

        InternalTestHelper.setInternalUserNumber(1000);
        UserServiceImpl userServiceImpl = new UserServiceImpl(gpsUtil, rewardsServiceImpl, attractionServiceImpl);

        rewardsServiceImpl.calculateRewards(userServiceImpl.getAllUsers().get(0));
        List<UserReward> userRewards = userServiceImpl.getUserRewards(userServiceImpl.getAllUsers().get(0));
        userServiceImpl.tracker.stopTracking();

        Assertions.assertEquals(attractionServiceImpl.getAttractions().size(), userRewards.size());
    }

}
