package tourGuide.service;

import gpsUtil.location.Attraction;

import java.util.List;

public interface AttractionService {
    List<Attraction> getAttractions();
}
