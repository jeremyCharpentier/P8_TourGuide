package tourGuide.service;

import gpsUtil.location.VisitedLocation;
import tourGuide.domain.User;
import tourGuide.domain.UserReward;
import tourGuide.dto.NearAttractionsDto;
import tripPricer.Provider;

import java.util.List;

public interface UserService {
    User getUser(String userName);

    List<User> getAllUsers();

    void addUser(User user);

    List<Provider> getTripDeals(User user);

    List<UserReward> getUserRewards(User user);

    List<VisitedLocation> trackUserLocationRunnable(List<User> users);

    VisitedLocation trackUserLocation(User user);

    List<NearAttractionsDto> getFiveClosesAttractions(VisitedLocation visitedLocation);
}
