package tourGuide.service;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AttractionServiceImpl implements AttractionService {

    private final List<Attraction> attractions;

    public AttractionServiceImpl() {
        GpsUtil gpsUtil = new GpsUtil();
        attractions = gpsUtil.getAttractions();
    }

    public List<Attraction> getAttractions() {
        return attractions;
    }
}
