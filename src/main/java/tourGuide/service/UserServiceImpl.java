package tourGuide.service;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import rewardCentral.RewardCentral;
import tourGuide.config.InitializeInternalUsers;
import tourGuide.domain.LocalizedAttraction;
import tourGuide.domain.User;
import tourGuide.domain.UserReward;
import tourGuide.dto.NearAttractionsDto;
import tourGuide.tracker.Tracker;
import tripPricer.Provider;
import tripPricer.TripPricer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@Service
@Slf4j
public class UserServiceImpl implements UserService {
    private final GpsUtil gpsUtil;
    private final RewardsServiceImpl rewardsServiceImpl;
    private final AttractionServiceImpl attractionServiceImpl;
    private final TripPricer tripPricer = new TripPricer();
    public final Tracker tracker;
    boolean testMode = true;

    private final InitializeInternalUsers initializeInternalUsers = new InitializeInternalUsers();

    private final ExecutorService executorService = Executors.newFixedThreadPool(100);

    public UserServiceImpl(GpsUtil gpsUtil, RewardsServiceImpl rewardsServiceImpl, AttractionServiceImpl attractionServiceImpl) {
        this.gpsUtil = gpsUtil;
        this.rewardsServiceImpl = rewardsServiceImpl;
        this.attractionServiceImpl = attractionServiceImpl;

        if (testMode) {
            log.info("TestMode enabled");
            log.debug("Initializing users");
            initializeInternalUsers.build();
            log.debug("Finished initializing users");
        }
        tracker = new Tracker(this);
        addShutDownHook();
    }

    public List<UserReward> getUserRewards(User user) {
        return user.getUserRewards();
    }

    public VisitedLocation getUserLocation(User user) {
        return (user.getVisitedLocations().size() > 0) ?
                user.getLastVisitedLocation() :
                trackUserLocationRunnable(Collections.singletonList(user)).get(0);
    }

    public User getUser(String userName) {
        return initializeInternalUsers.getInternalUserMap().get(userName);
    }

    public List<User> getAllUsers() {
        return new ArrayList<>(initializeInternalUsers.getInternalUserMap().values());
    }

    public void addUser(User user) {

        if (!initializeInternalUsers.getInternalUserMap().containsKey(user.getUserName())) {
            initializeInternalUsers.getInternalUserMap().put(user.getUserName(), user);
        }
    }

    public List<Provider> getTripDeals(User user) {
        int cumulativeRewardPoints = user.getUserRewards().stream().mapToInt(UserReward::getRewardPoints).sum();

        List<Provider> providers = tripPricer.getPrice(initializeInternalUsers.getTripPricerApiKey(), user.getUserId(), user.getUserPreferences().getNumberOfAdults(),
                user.getUserPreferences().getNumberOfChildren(), user.getUserPreferences().getTripDuration(), cumulativeRewardPoints);

        user.setTripDeals(providers);
        return providers;
    }

    public VisitedLocation trackUserLocation(User user) {
        VisitedLocation visitedLocation = gpsUtil.getUserLocation(user.getUserId());

        user.addToVisitedLocations(visitedLocation);
        rewardsServiceImpl.completableFutureCalculateRewards(Collections.singletonList(user));
        return visitedLocation;
    }

    public List<VisitedLocation> trackUserLocationRunnable(List<User> users) {
        List<CompletableFuture<VisitedLocation>> completableFutures = users
                .parallelStream()
                .map(user -> CompletableFuture.supplyAsync(() -> trackUserLocation(user), executorService))
                .collect(toList());

        return completableFutures.parallelStream().map(CompletableFuture::join).collect(Collectors.toList());
    }

    public List<NearAttractionsDto> getFiveClosesAttractions(VisitedLocation visitedLocation) {
        int attractionsCount = 5;

        List<Attraction> attractions = attractionServiceImpl.getAttractions()
                .stream()
                .map(attraction -> new LocalizedAttraction(attraction, visitedLocation, rewardsServiceImpl))
                .sorted(Comparator.comparing(LocalizedAttraction::getDistance))
                .limit(attractionsCount)
                .collect(Collectors.toList());

        List<NearAttractionsDto> nearAttractionsDto = new ArrayList<>();

        for (Attraction attraction : attractions) {
            List<NearAttractionsDto> aggregate =
                    attractions.stream()
                            .map(attraction1 -> new NearAttractionsDto(attraction, visitedLocation, new LocalizedAttraction(attraction, visitedLocation, rewardsServiceImpl), new RewardCentral()))
                            .limit(1)
                            .collect(Collectors.toList());

            nearAttractionsDto.addAll(aggregate);
        }
        return nearAttractionsDto;
    }

    private void addShutDownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread(tracker::stopTracking));
    }
}
