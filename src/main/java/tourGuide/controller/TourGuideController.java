package tourGuide.controller;

import com.jsoniter.output.JsonStream;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tourGuide.domain.User;
import tourGuide.domain.UserReward;
import tourGuide.service.UserServiceImpl;
import tripPricer.Provider;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class TourGuideController {

    @Autowired
    UserServiceImpl userServiceImpl;

    @RequestMapping("/")
    public String index() {
        return "Greetings from TourGuide!";
    }

    @RequestMapping("/getLocation")
    public String getLocation(@RequestParam String userName) {
        VisitedLocation visitedLocation = userServiceImpl.getUserLocation(getUser(userName));
        return JsonStream.serialize(visitedLocation.location);
    }

    @RequestMapping("/getNearbyAttractions")
    public String getNearbyAttractions(@RequestParam String userName) {
        VisitedLocation visitedLocation = userServiceImpl.getUserLocation(getUser(userName));
        return JsonStream.serialize(userServiceImpl.getFiveClosesAttractions(visitedLocation));
    }

    @RequestMapping("/getRewards")
    public String getRewards(@RequestParam String userName) {
        List<UserReward> userRewards = userServiceImpl.getUserRewards(getUser(userName));
        return JsonStream.serialize(userRewards);
    }

    @RequestMapping("/getAllCurrentLocations")
    public String getAllCurrentLocations() {
        List<User> users = userServiceImpl.getAllUsers();
        Map<String, Location> userLocations = new HashMap<>();

        for (User user : users) {
            userLocations.put(user.getUserId().toString(), user.getLastVisitedLocation().location);
        }

        return JsonStream.serialize(userLocations);
    }

    @RequestMapping("/getTripDeals")
    public String getTripDeals(@RequestParam String userName) {
        List<Provider> providers = userServiceImpl.getTripDeals(getUser(userName));
        return JsonStream.serialize(providers);
    }

    private User getUser(String userName) {
        return userServiceImpl.getUser(userName);
    }

}