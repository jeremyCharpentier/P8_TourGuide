package tourGuide.dto;

import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import rewardCentral.RewardCentral;
import tourGuide.domain.LocalizedAttraction;

public class NearAttractionsDto {
    private final String attractionName;
    private final double latitudeAttraction;
    private final double longitudeAttraction;
    private final double latitudeLocationVisited;
    private final double longitudeLocationVisited;
    private final double distance;
    private final int attractionRewardPoints;

    public NearAttractionsDto(Attraction attraction, VisitedLocation visitedLocation, LocalizedAttraction localizedAttraction, RewardCentral rewardCentral) {
        this.attractionName = attraction.attractionName;
        this.latitudeAttraction = attraction.latitude;
        this.longitudeAttraction = attraction.longitude;
        this.latitudeLocationVisited = visitedLocation.location.latitude;
        this.longitudeLocationVisited = visitedLocation.location.longitude;
        this.distance = localizedAttraction.getDistance();
        this.attractionRewardPoints = rewardCentral.getAttractionRewardPoints(attraction.attractionId, visitedLocation.userId);
    }

    public String getAttractionName() {
        return attractionName;
    }

    public double getLatitudeAttraction() {
        return latitudeAttraction;
    }

    public double getLongitudeAttraction() {
        return longitudeAttraction;
    }

    public double getLatitudeLocationVisited() {
        return latitudeLocationVisited;
    }

    public double getLongitudeLocationVisited() {
        return longitudeLocationVisited;
    }

    public double getDistance() {
        return distance;
    }

    public int getAttractionRewardPoints() {
        return attractionRewardPoints;
    }
}
