package tourGuide.domain;

import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import tourGuide.service.RewardsServiceImpl;

public class LocalizedAttraction extends Attraction {

    private final double distance;

    public LocalizedAttraction(Attraction attraction, VisitedLocation visitedLocation, RewardsServiceImpl rewardsServiceImpl) {
        super(attraction.attractionName, attraction.city, attraction.state, attraction.longitude, attraction.latitude);
        this.distance = rewardsServiceImpl.getDistance(attraction, visitedLocation.location);
    }

    public double getDistance() {
        return this.distance;
    }
}
