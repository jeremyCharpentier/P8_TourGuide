package tourGuide.config;

import gpsUtil.GpsUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import rewardCentral.RewardCentral;
import tourGuide.service.AttractionServiceImpl;
import tourGuide.service.RewardsServiceImpl;

@Configuration
public class TourGuideModule {

    @Bean
    public GpsUtil getGpsUtil() {
        return new GpsUtil();
    }

    @Bean
    public RewardsServiceImpl getRewardsService() {
        return new RewardsServiceImpl(getRewardCentral(), new AttractionServiceImpl());
    }

    @Bean
    public RewardCentral getRewardCentral() {
        return new RewardCentral();
    }

    @Bean
    InitializeInternalUsers initializeInternalUsers() {
        return new InitializeInternalUsers();
    }
}
